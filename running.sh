#!/usr/bin/env bash
###########################################
# owner: droritzz
# purpose: basic script for jenkins pipeline
# date: 6.7.21
# version: v1.0.0.1
###########################################

time=0
while [[ $time -lt 40 ]]
do
	echo "running"
	sleep 5
	time=$((time + 5))
done
