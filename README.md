## This project contains scripts and files to learn Jenkins pipeline, jenkinsfile and git integration.

- script that runs for 40 seconds in a loop
- jenkins file that runs the loop script in a pipeline
- integration between the git and jenkins


##  To use this project:
- install Jenkins using this [repo lab](https://gitlab.com/droritzz/jenkis-labs) and all plugins
- login into Jenkins and create a pipeline using this [tutorial](https://www.thegeekstuff.com/2021/01/create-jenkins-pipeline/). The third method explains how to pull Jenkinsfile from the GitLab manually.
